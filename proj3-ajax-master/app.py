"""
Very simple Flask web site, with one page
displaying a course schedule.

"""

import flask
from flask import render_template
from flask import request
from flask import url_for
from flask import jsonify  # For AJAX transactions

import json
import logging
import math

# Date handling 
import arrow  # Replacement for datetime, based on moment.js
import datetime  # But we still need time
from dateutil import tz  # For interpreting local times

# Our own module
# import acp_limits


# ##
# Globals
###
app = flask.Flask(__name__)
import CONFIG

import uuid

app.secret_key = str(uuid.uuid4())
app.debug = CONFIG.DEBUG
app.logger.setLevel(logging.DEBUG)


###
# Pages
###

@app.route("/")
@app.route("/index")
@app.route("/calc") # on local host
# @app.route("calc")
@app.route("/calc.html")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('page_not_found.html'), 404


###############
#
# AJAX request handlers 
#   These return JSON, rather than rendering pages. 
#
###############


class Values():
    maxhour = 0
    maxmin = 0
    minhour = 0
    minmin = 0
    brevetdis = 200
    unit = 1


@app.route("/_update_length")
def update_length():
    app.logger.debug("Got a JSON request")
    brevetdis = request.args.get('distance', 0, type=int)
    Values.brevetdis = brevetdis
    return jsonify(result="{}".format(brevetdis))


@app.route("/_update_unit")
def update_unit():
    app.logger.debug("Got a JSON request")
    unit = request.args.get('unit', 0, type=str)
    if unit == "km":
        Values.unit = 1
    else:
        Values.unit = 0.62137119
    return jsonify(result="{}".format(unit))


def calc_helper(remaining, min, max):
    tempminhour = remaining / min
    tempminmin = (tempminhour - math.floor(tempminhour)) * 60
    tempmaxhour = remaining / max
    tempmaxmin = (tempmaxhour - math.floor(tempmaxhour)) * 60

    Values.minhour += tempminhour
    Values.minmin += tempminmin
    Values.maxhour += tempmaxhour
    Values.maxmin += tempmaxmin


def result_helper():
    Values.minhour = math.floor(Values.minhour)
    Values.maxhour = math.floor(Values.maxhour)
    if round(Values.maxmin) <= 9 and round(Values.minmin) <= 9:
        result = "{}{}{}{}{}{}{}".format(Values.minhour, ":0", round(Values.minmin), " - ", Values.maxhour,
                                         ":0", round(Values.maxmin))
    elif round(Values.maxmin) <= 9:
        result = "{}{}{}{}{}{}{}".format(Values.minhour, ":", round(Values.minmin), " - ", Values.maxhour,
                                         ":0", round(Values.maxmin))
    elif round(Values.minmin) <= 9:
        result = "{}{}{}{}{}{}{}".format(Values.minhour, ":0", round(Values.minmin), " - ", Values.maxhour,
                                         ":", round(Values.maxmin))
    else:
        result = "{}{}{}{}{}{}{}".format(Values.minhour, ":", round(Values.minmin), " - ", Values.maxhour,
                                         ":", round(Values.maxmin))
    return result


@app.route("/_calc_times")
def calc_times():
    """
    Expects one URL-encoded argument, the number of miles.
    """

    app.logger.debug("Got a JSON request")
    miles = request.args.get('miles', 0, type=int)

    remaining = miles / Values.unit
    Values.maxhour = 0
    Values.maxmin = 0
    Values.minhour = 0
    Values.minmin = 0

    if (Values.brevetdis + (Values.brevetdis / 5)) >= remaining >= Values.brevetdis:
        if Values.brevetdis == 1000:
            return jsonify(result="{}".format("09:05 - 03:00"))
        elif Values.brevetdis == 600:
            return jsonify(result="{}".format("18:48 - 16:00"))
        elif Values.brevetdis == 400:
            return jsonify(result="{}".format("12:08 - 03:00"))
        elif Values.brevetdis == 300:
            return jsonify(result="{}".format("09:00 - 20:00"))
        elif Values.brevetdis == 200:
            return jsonify(result="{}".format("05:53 - 13:30"))
    if miles < 0:
        return jsonify(result="{}".format("miles too low"))

    if miles > (Values.brevetdis + (Values.brevetdis / 5)):
        return jsonify(result="{}".format("miles too high"))

    if remaining > Values.brevetdis:
        remaining = Values.brevetdis

    if 1000 < remaining <= 1300:
        temp = remaining - 1000
        remaining -= temp
        calc_helper(remaining, 26, 13.333)

    if 600 < remaining <= 1000:
        temp = remaining - 600
        remaining -= temp
        calc_helper(remaining, 28, 11.428)

    if 400 < remaining <= 600:
        temp = remaining - 400
        remaining -= temp
        calc_helper(remaining, 30, 15)

    if 200 < remaining <= 400:
        temp = remaining - 200
        remaining -= temp
        calc_helper(remaining, 32, 15)

    if 0 < remaining <= 200:
        calc_helper(remaining, 34, 15)

    while Values.minmin >= 60.0:
        Values.minmin -= 60.0

    while Values.maxmin >= 60.0:
        Values.maxmin -= 60.0

    while Values.minhour >= 24.0:
        Values.minhour -= 24.0

    while Values.maxhour >= 24.0:
        Values.maxhour -= 24.0

    return jsonify(result="{}".format(result_helper()))


#################
#
# Functions used within the templates
#
#################

@app.template_filter('fmtdate')
def format_arrow_date(date):
    try:
        normal = arrow.get(date)
        return normal.format("ddd YYYY/DD/MM")
    except:
        return "(bad date)"


@app.template_filter('fmttime')
def format_arrow_time(time):
    try:
        normal = arrow.get(date)
        return normal.format("hh:mm")
    except:
        return "(bad time)"


#############


if __name__ == "__main__":
    import uuid

    app.secret_key = str(uuid.uuid4())
    app.debug = CONFIG.DEBUG
    app.logger.setLevel(logging.DEBUG)
    app.run(port=CONFIG.PORT)
