#! /usr/bin/env python3

""" For deployment on ix under CGI """

import site
site.addsitedir("/home/users/nhasson/public_html/htbin/uoregon-cis399/proj2-flask-master/env/lib/python3.5/site-packages")

from wsgiref.handlers import CGIHandler
from syllabus import app

CGIHandler().run(app)
